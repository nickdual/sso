class UserController < ApplicationController
  before_filter :authenticate_user!
  before_filter :detect_xrds, :only => :show

  def show
    @account = User.where(:username => (params[:user] || request.subdomain), :confirmation_token => nil).first || not_found
    #raise ActiveRecord::RecordNotFound if @account.nil?

    respond_to do |format|
      format.html do
        response.headers['X-XRDS-Location'] = identity_url(:user => @account.username, :format => :xrds, :protocol => scheme)
        render :template => 'accounts/show'
      end
      format.xrds do
        render :template => 'accounts/show'
      end
    end
  end

  # GET
  def edit
    @user = current_user
    render :template => 'accounts/edit'
  end

  # PUT
  def update
    respond_to do |format|
      if current_user.update_attributes(params[:user])
        format.html { redirect_to '/user/edit', notice: 'User was successfully updated'}
        format.json { render json: {:status => :ok}}
      else
        format.html { render action: "edit" }
        format.json { render json: current_user.errors.to_json}
      end
    end
  end

  protected

  def not_found
    raise ActionController::RoutingError.new('User Not Found')

  end

  def detect_xrds
    if params[:user] =~ /\A(.+)\.xrds\z/
      request.format = :xrds
      params[:user] = $1
    end
  end
end
