class ApplicationController < ActionController::Base
  include OpenidServerSystem

  protect_from_forgery

  helper_method :extract_host, :extract_login_from_identifier, :checkid_request,
                :identifier, :endpoint_url, :scheme, :email_as_login?

  protected

  def after_sign_in_path_for(resource)
    session[:return_to] || (request.path == '/users/sign_in' ? '/' : request.referrer)
  end

  def endpoint_url
    server_url(:protocol => scheme)
  end

  # Returns the OpenID identifier for an account
  def identifier(account)
    identity_url(:user => account.username, :protocol => scheme)
  end

  # Extracts the hostname from the given url, which is used to
  # display the name of the requesting website to the user
  def extract_host(u)
    URI.split(u).compact[1]
  end

  def extract_login_from_identifier(openid_url)
    openid_url.gsub(/^https?:\/\/.*\//, '')
  end

  def checkid_request
    unless @checkid_request
      req = openid_server.decode_request(current_openid_request["parameters"]) if current_openid_request
      @checkid_request = req.is_a?(OpenID::Server::CheckIDRequest) ? req : false
    end
    @checkid_request
  end

  def current_openid_request
    @current_openid_request ||= OpenIdRequest.where(:token => session[:request_token]).first if session[:request_token]
  end

  def render_404
    render_error(404)
  end

  def render_422
    render_error(422)
  end

  def render_500
    render_error(500)
  end

  def render_error(status_code)
    render :file => "#{Rails.root}/public/#{status_code}", :formats => [:html], :status => status_code, :layout => false
  end

  private

  def scheme
    'http'
  end

  def email_as_login?
    false
  end
end
