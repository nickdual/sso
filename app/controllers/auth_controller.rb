class AuthController < ApplicationController
  layout 'raw'

  def index

  end

  def global
    @token = ''
    if (user_signed_in?)
      @token = {:username => current_user.username, :email => current_user.email, :encrypted_password => current_user.encrypted_password}
      @nonce = current_user.current_sign_in_at.to_i
      @token = ActiveSupport::MessageEncryptor.new(SsoServer::Application.config.sso_config["key"]).encrypt_and_sign(@token)
    end
  end

  def get_profile
    render json: {"username" => "MQuy"}
  end

  def update_profile

  end
end