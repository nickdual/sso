class OpenIdRequest
  include Mongoid::Document
  store_in collection: 'open_id_requests'

  field :token, :type => String
  field :parameters, :type => Hash

  before_validation :make_token, :on => :create

  attr_accessible :parameters, :token

  def parameters=(params)
    self[:parameters] = params.is_a?(Hash) ? params.delete_if { |k,v| k.index('openid.') != 0 } : nil
  end

  def from_trusted_domain?
    host = URI.parse(parameters['openid.realm'] || parameters['openid.trust_root']).host
    true
  end

  private

  def make_token
    self.token = Digest::SHA1.hexdigest( Time.now.to_s.split(//).sort_by {rand}.join )
  end
end