class UserActivity
  include Mongoid::Document
  store_in collection: 'user_activities'

  #Relation
  belongs_to :user

  field :ip, :type => String
  field :date, :type => Time
  field :type, :type => String
  field :description, :type => String
end
