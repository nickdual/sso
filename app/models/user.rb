class User
  include Mongoid::Document
  validates_format_of :username, with: /^[a-zA-Z0-9_]+$/, message: "must be lowercase alphanumerics only"
  validates_length_of :username, maximum: 32, message: "exceeds maximum of 32 characters"
  validates_exclusion_of :username, in: ['www', 'mail', 'ftp'], message: "is not available"

  #Relation
  has_one :user_activities
  embeds_many :user_profiles, store_as: "profiles"
  embeds_many :user_trust_sites, store_as: "trust_sites"

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :registerable, :confirmable, :authentication_keys => [:login]
  ## Database authenticatable
  field :username, :type => String, :default => ""
  field :email,              :type => String, :default => ""
  field :encrypted_password, :type => String, :default => ""
  
  ## Recoverable
  field :reset_password_token,   :type => String
  field :reset_password_sent_at, :type => Time

  ## Rememberable
  field :remember_created_at, :type => Time

  ## Trackable
  field :sign_in_count,      :type => Integer, :default => 0
  field :current_sign_in_at, :type => Time
  field :last_sign_in_at,    :type => Time
  field :current_sign_in_ip, :type => String
  field :last_sign_in_ip,    :type => String

  ## Confirmable
  field :confirmation_token,   :type => String
  field :confirmed_at,         :type => Time
  field :confirmation_sent_at, :type => Time
  field :unconfirmed_email,    :type => String # Only if using reconfirmable

  ## Lockable
  # field :failed_attempts, :type => Integer, :default => 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    :type => String # Only if unlock strategy is :email or :both
  # field :locked_at,       :type => Time

  ## Token authenticatable
  # field :authentication_token, :type => String

  #Extra Attributes
  mount_uploader :image, ImagesUploader
  field :image_original_name, :type => String
  field :nickname, :type => String
  field :update_at, :type => Time


  attr_accessor :login
  attr_accessible :login

  attr_accessible :username, :email, :password, :password_confirmation, :remember_me, :sign_in_count, :image, :image_cache

  def to_jq_upload
    {
        "name" => read_attribute(:image),
        "size" => image.size,
        "url" => image.url,
        "thumbnail_url" => image.thumb.url
    }
  end

  # function to handle user's login via email or username
  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login).downcase
      where(conditions).where('$or' => [ {:username => /^#{Regexp.escape(login)}$/i}, {:email => /^#{Regexp.escape(login)}$/i} ]).first
    else
      where(conditions).first
    end
  end

  def drop_the_case
    self.username = self.username.downcase
  end

  def sreg_properties(profile_id)
    props = {}
    profile = user_profiles.where(:id => profile_id).first
    profile.attributes.each do |item, index|
      props[index] = item if SsoServer::Application.config.sso_config["attribute_mappings"][index].include?("://") == false
    end
    props
  end

  def ax_properties(profile_id)
    props = {}
    profile = user_profiles.where(:id => profile_id).first
    profile.attributes.each do |item, index|
      props["type.#{index}"] = SsoServer::Application.config.sso_config["attribute_mappings"][index].last
      props["value.#{index}"] = item
    end
    props
  end
end


class UserProfile
  include Mongoid::Document
  #Relation
  embedded_in :user

  field :title, :type => String
  field :fullname, :type => String
  field :nickname, :type => String
  field :date_of_birth, :type => Date
  field :gender, :type => String
  field :language, :type => String
  field :phone, :type => String
  field :im_skype, :type => String
  field :biography, :type => String
  field :address, :type => String
  field :city, :type => String
  field :country, :type => String
  field :company_name, :type => String
  field :job_title, :type => String
  field :image, :type => String
end

class UserTrustSite
  include Mongoid::Document
  #Relation
  embedded_in :user

  field :hostname, :type => String
  field :status, :type => Integer
  field :profile_id, :type => String
  field :sign_in_at, :type => Time
end