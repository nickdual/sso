xml.instruct!
xml.personaConfig(:serverIdentifier => endpoint_url, :version => '1.0') do
  xml.persona(identifier(current_user), :displayName => current_user.username) if logged_in?
end