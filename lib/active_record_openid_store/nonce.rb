
class Nonce
  include Mongoid::Document
  store_in collection: 'open_id_nonces'

  field :server_url, :type => String
  field :timestamp, :type => String
  field :salt, :type => String

  attr_accessible :server_url, :timestamp, :salt
end
